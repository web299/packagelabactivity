package secondpackage;

/**
 *	Utilities is a class used for testing packages out
 *  information about a rectangle
 *  @author   Lion
 *  @version  8/29/2022
*/
public class Utilities {
    /**
     *  Doubles the input value
     *
     *  @param  x   The value to be doubled
     *  @return The value of x times two
     */
    public int doubleMe(int x){
        return x*2;
    }
}
