package hellopackage;
/*import java.util.Scanner;
import java.util.Random;*/
import secondpackage.Utilities;
import java.util.*;
/**
 *	Greatr is a class used for testing packages out
 *  information about a rectangle
 *  @author   Lion
 *  @version  8/29/2022
*/

public class Greater{
    

    public static void main(String[] args){
        Scanner reader= new Scanner(System.in);
        //Random randomiser = new Random();
        System.out.println("Enter a value to double it:");
        int var= reader.nextInt();
        Utilities myVar= new Utilities();
         var=myVar.doubleMe(var);
         System.out.println("The doubled value is: "+var);
       
    }
}